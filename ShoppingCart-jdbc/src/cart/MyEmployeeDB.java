package cart;

import entity.EmployeeDTO;
import java.io.IOException;
import java.util.ArrayList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author leoseo
 */
public class MyEmployeeDB {

    public void createEmployeeTable() {
        Connection cnnct = null;
        Statement stmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            stmnt = cnnct.createStatement();
            stmnt.execute("CREATE TABLE EMPLOYEE ( "
                    + "EMPID CHAR(6) , "
                    + "NAME VARCHAR(50), "
                    + "PASSWORD CHAR(64), "
                    + "EMAIL VARCHAR(50), "
                    + "PHONE VARCHAR(10), "
                    + "ADDRESS VARCHAR(50), "
                    + "APPGROUP VARCHAR(20), "
                    + "ACTIVE BOOLEAN,"
                    + "MEMO VARCHAR(255), "
                    + "CONSTRAINT PK_CART_EMPLOYEE PRIMARY KEY (EMPID))");

        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (stmnt != null) {
                try {
                    stmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }

    public void dropEmployeeTable() {
        Connection cnnct = null;
        Statement stmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            stmnt = cnnct.createStatement();
            stmnt.execute("DROP TABLE EMPLOYEE");
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (stmnt != null) {
                try {
                    stmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }

    public void addRecords(ArrayList<EmployeeDTO> emsEmployees) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            String preQueryStatement
                    = "INSERT INTO EMPLOYEE VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pStmnt = cnnct.prepareStatement(preQueryStatement);

            for (EmployeeDTO emsEmployee : emsEmployees) {
                pStmnt.setString(1, emsEmployee.getEmpid());
                pStmnt.setString(2, emsEmployee.getName());
                pStmnt.setString(3, emsEmployee.getPassword());
                pStmnt.setString(4, emsEmployee.getEmail());
                pStmnt.setString(5, emsEmployee.getPhone());
                pStmnt.setString(6, emsEmployee.getAddress());
                pStmnt.setString(7, emsEmployee.getAppgroup());
                pStmnt.setBoolean(8, emsEmployee.isActive());
                pStmnt.setString(9, emsEmployee.getMemo());

                int rowCount = pStmnt.executeUpdate();
                if (rowCount == 0) {
                    throw new SQLException("Cannot insert records!");
                }
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (pStmnt != null) {
                try {
                    pStmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }
}
