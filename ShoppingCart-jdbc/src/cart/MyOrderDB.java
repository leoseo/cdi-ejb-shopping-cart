/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cart;

import entity.OrderDTO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author leoseo
 */
public class MyOrderDB {
    
    public void createOrderTable() {
        Connection cnnct = null;
        Statement stmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            stmnt = cnnct.createStatement();
            stmnt.execute("CREATE TABLE CART_ORDER ( "
                    + "ORDERID CHAR(6) , "
                    + "CUSTOMERNAME VARCHAR(50), "
                    + "CUSTOMEREMAIL VARCHAR(54), "
                    + "CUSTOMERPHONE VARCHAR(10), "
                    + "CUSTOMERADDRESS VARCHAR(50), "
                    + "TOTALCOST DECIMAL(10,2), "
                    + "PROCESSED BOOLEAN, "
                    + "CONSTRAINT PK_ORDER PRIMARY KEY (ORDERID))");

        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (stmnt != null) {
                try {
                    stmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }

    public void dropOrderTable() {
        Connection cnnct = null;
        Statement stmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            stmnt = cnnct.createStatement();
            stmnt.execute("DROP TABLE CART_ORDER");
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (stmnt != null) {
                try {
                    stmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }

    public void addRecords(ArrayList<OrderDTO> orders) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            String preQueryStatement
                    = "INSERT INTO CART_ORDER VALUES (?, ?, ?, ?, ?, ?, ?)";
            pStmnt = cnnct.prepareStatement(preQueryStatement);

            for (OrderDTO order : orders) {
                pStmnt.setString(1, order.getOrderid());
                pStmnt.setString(2, order.getCustomerName());
                pStmnt.setString(3, order.getCustomerEmail());
                pStmnt.setString(4, order.getCustomerPhone());
                pStmnt.setString(5, order.getCustomerAddress());
                pStmnt.setDouble(6, order.getTotalCost().doubleValue());
                pStmnt.setBoolean(7, order.isProcessed());

                int rowCount = pStmnt.executeUpdate();
                if (rowCount == 0) {
                    throw new SQLException("Cannot insert records!");
                }
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (pStmnt != null) {
                try {
                    pStmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }
    
}
