/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cart;

import entity.OrderProductDTO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author leoseo
 */
public class MyOrderProductDB {
    
    public void createTable() {
        Connection cnnct = null;
        Statement stmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            stmnt = cnnct.createStatement();
            stmnt.execute("CREATE TABLE CART_PRODUCT_ORDER ( "
                    + "ORDERID CHAR(6) , "
                    + "PRODUCTID CHAR(6), "
                    + "QTYORDER DECIMAL(5), "
                    + "CONSTRAINT PK_PRODUCT_ORDER PRIMARY KEY (ORDERID, PRODUCTID))");

        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (stmnt != null) {
                try {
                    stmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }

    public void dropTable() {
        Connection cnnct = null;
        Statement stmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            stmnt = cnnct.createStatement();
            stmnt.execute("DROP TABLE CART_PRODUCT_ORDER");
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (stmnt != null) {
                try {
                    stmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }

    public void addRecords(ArrayList<OrderProductDTO> items) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            String preQueryStatement
                    = "INSERT INTO CART_PRODUCT_ORDER VALUES (?, ?, ?)";
            pStmnt = cnnct.prepareStatement(preQueryStatement);

            for (OrderProductDTO item : items) {
                pStmnt.setString(1, item.getOrderid());
                pStmnt.setString(2, item.getProductid());
                pStmnt.setInt(3, item.getQtyOrder());

                int rowCount = pStmnt.executeUpdate();
                if (rowCount == 0) {
                    throw new SQLException("Cannot insert records!");
                }
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (pStmnt != null) {
                try {
                    pStmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }
    
}
