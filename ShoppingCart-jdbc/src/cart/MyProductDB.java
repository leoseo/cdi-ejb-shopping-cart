/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cart;

import entity.CartProductDTO;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author leoseo
 */
public class MyProductDB {

    public void createProductTable() {
        Connection cnnct = null;
        Statement stmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            stmnt = cnnct.createStatement();
            stmnt.execute("CREATE TABLE CART_PRODUCT ( "
                    + "PRODUCTID CHAR(6), "
                    + "PRODUCTNAME VARCHAR(50), "
                    + "DESCRIPTION VARCHAR(100), "
                    + "UNITPRICE DECIMAL(10,2), "
                    + "QTYONHAND DECIMAL(5), "
                    + "THRESHOLD DECIMAL(5), "
                    + "MEMO VARCHAR(255), "
                    + "ACTIVE BOOLEAN, "
                    + "CONSTRAINT PK_CART_PRODUCT PRIMARY KEY (PRODUCTID))");

        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (stmnt != null) {
                try {
                    stmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }

    public void dropProductTable() {
        Connection cnnct = null;
        Statement stmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            stmnt = cnnct.createStatement();
            stmnt.execute("DROP TABLE CART_PRODUCT");
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (stmnt != null) {
                try {
                    stmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }

    public void addRecords(ArrayList<CartProductDTO> products) {
        Connection cnnct = null;
        PreparedStatement pStmnt = null;
        try {
            cnnct = ConnectionConfig.getConnection();
            String preQueryStatement
                    = "INSERT INTO CART_PRODUCT VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
            pStmnt = cnnct.prepareStatement(preQueryStatement);

            for (CartProductDTO product : products) {
                pStmnt.setString(1, product.getProductid());
                pStmnt.setString(2, product.getProductname());
                pStmnt.setString(3, product.getDescription());
                pStmnt.setDouble(4, product.getUnitprice());
                pStmnt.setInt(5, product.getQtyonhand());
                pStmnt.setInt(6, product.getThresholdqty());
                pStmnt.setString(7, product.getMemo());
                pStmnt.setBoolean(8, product.isActive());

                int rowCount = pStmnt.executeUpdate();
                if (rowCount == 0) {
                    throw new SQLException("Cannot insert records!");
                }
            }
        } catch (SQLException ex) {
            while (ex != null) {
                ex.printStackTrace();
                ex = ex.getNextException();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (pStmnt != null) {
                try {
                    pStmnt.close();
                } catch (SQLException e) {
                }
            }
            if (cnnct != null) {
                try {
                    cnnct.close();
                } catch (SQLException sqlEx) {
                }
            }
        }
    }
}
