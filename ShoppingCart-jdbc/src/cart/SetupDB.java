/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cart;

import entity.EmployeeDTO;
import entity.CartProductDTO;
import entity.OrderDTO;
import entity.OrderProductDTO;
import java.math.BigDecimal;
import java.util.ArrayList;
import utils.HashUtils;

/**
 *
 * @author leoseo
 */
public class SetupDB {

    public static void main(String[] args) {
        MyEmployeeDB empDB = new MyEmployeeDB();
        empDB.dropEmployeeTable();
        empDB.createEmployeeTable();
        empDB.addRecords(prepareEmployeeData());
        
        MyProductDB prodDB = new MyProductDB();
        prodDB.dropProductTable();
        prodDB.createProductTable();
        prodDB.addRecords(prepareProductData());
        
        MyOrderDB orderDB = new MyOrderDB();
        orderDB.dropOrderTable();
        orderDB.createOrderTable();
        orderDB.addRecords(prepareOrderData());
        
        MyOrderProductDB itemDB = new MyOrderProductDB();
        itemDB.dropTable();
        itemDB.createTable();
        itemDB.addRecords(prepareItemData());
        
        System.out.println("Refreshed DB data!");
    }

    private static ArrayList<EmployeeDTO> prepareEmployeeData() {

        ArrayList<EmployeeDTO> empList = new ArrayList<>();

        String memo1 = "123456";
        String pwd1 = HashUtils.getSHA256HashedString(memo1);
        EmployeeDTO e1 = new EmployeeDTO("0001", "admin01", pwd1,
                "admin1@gmail.com", "0123456789", "Melbourne, VIC",
                "ROLE_ADMINISTRATOR", true, memo1);
        empList.add(e1);
        
        String memo2 = "123456";
        String pwd2 = HashUtils.getSHA256HashedString(memo2);
        EmployeeDTO e2 = new EmployeeDTO("0002", "admin02", pwd2,
                "admin2@gmail.com", "0987654321", "Melbourne, VIC",
                "ROLE_ADMINISTRATOR", false, memo2);
        empList.add(e2);
        
        String memo3 = "999999";
        String pwd3 = HashUtils.getSHA256HashedString(memo3);
        EmployeeDTO e3 = new EmployeeDTO("0003", "staff01", pwd3,
                "staff1@gmail.com", "09999999", "Melbourne, VIC",
                "ROLE_USER", true, memo3);
        empList.add(e3);
        
        String memo4 = "888888";
        String pwd4 = HashUtils.getSHA256HashedString(memo4);
        EmployeeDTO e4 = new EmployeeDTO("0004", "staff02", pwd4,
                "staff2@gmail.com", "08888888", "Melbourne, VIC",
                "ROLE_USER", false, memo4);
        empList.add(e4);
        
        return empList;
    }
    
    public static ArrayList<CartProductDTO> prepareProductData() {
        
        ArrayList<CartProductDTO> productList = new ArrayList<>();
        
        CartProductDTO product1 = new CartProductDTO("P00001", "Instant Noodle", 
                "Chinese instant noodle", 20, 30, 10, "Demand 15 per month", true);
        productList.add(product1);
        
        CartProductDTO product2 = new CartProductDTO("P00002", "Soy Sauce", 
                "Vietnamese soy sauce", 30, 30, 10, "Demand 20 per month", true);
        productList.add(product2);
        
        CartProductDTO product3 = new CartProductDTO("P00003", "Fish Sauce", 
                "Vietnamese fish sauce", 40, 30, 10, "Demand 30 per month", false);
        productList.add(product3);
        
        CartProductDTO product4 = new CartProductDTO("P00004", "Steamed Buns", 
                "Chinese Steamed Buns", 15, 30, 10, "Demand 20 per month", true);
        productList.add(product4);
        
        CartProductDTO product5 = new CartProductDTO("P00005", "Steamed Dumpling", 
                "Hong Kong Steamed Dumpling", 25, 30, 10, "Demand 25 per month, increasing", true);
        productList.add(product5);

        return productList;
    }
    
    public static ArrayList<OrderDTO> prepareOrderData() {
        
        ArrayList<OrderDTO> orderList = new ArrayList<>();
        
        OrderDTO order1 = new OrderDTO("000001", "Son Dang", "lson.dang2706@gmail.com",
                "0123456789", "99 Anywhere, Melbourne", new BigDecimal ("9000"), true);
        orderList.add(order1);
        
        OrderDTO order2 = new OrderDTO("000002", "Son Dang", "lson.dang2706@gmail.com",
                "0123456789", "99 Anywhere, Melbourne", new BigDecimal ("1000"), true);
        orderList.add(order2);
        
        OrderDTO order3 = new OrderDTO("000003", "Test Customer", "lson.dang2706@gmail.com",
                "0123456789", "99 Anywhere, Melbourne", new BigDecimal ("5000"), true);
        orderList.add(order3);
        
        OrderDTO order4 = new OrderDTO("000004", "Another Test Customer", "lson.dang2706@gmail.com",
                "0123456789", "99 Anywhere, Melbourne", new BigDecimal ("900"), true);
        orderList.add(order4);
        
        OrderDTO order5 = new OrderDTO("000005", "Tony Stark", "lson.dang2706@gmail.com",
                "0123456789", "99 Anywhere, Melbourne", new BigDecimal ("800"), false);
        orderList.add(order5);
        
        return orderList;
    }
    
    public static ArrayList<OrderProductDTO> prepareItemData() {
        
        ArrayList<OrderProductDTO> itemList = new ArrayList<>();
        
        OrderProductDTO item1 = new OrderProductDTO("000001", "P00001", 1);
        itemList.add(item1);
        
        OrderProductDTO item2 = new OrderProductDTO("000001", "P00002", 2);
        itemList.add(item2);
        
        OrderProductDTO item3 = new OrderProductDTO("000002", "P00001", 3);
        itemList.add(item3);
        
        OrderProductDTO item4 = new OrderProductDTO("000002", "P00004", 4);
        itemList.add(item4);
        
        OrderProductDTO item5 = new OrderProductDTO("000003", "P00005", 5);
        itemList.add(item5);
        
        OrderProductDTO item6 = new OrderProductDTO("000004", "P00001", 6);
        itemList.add(item6);
        
        OrderProductDTO item7 = new OrderProductDTO("000005", "P00003", 7);
        itemList.add(item7);
        
        return itemList;
    }

}
