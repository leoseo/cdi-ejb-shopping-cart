/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author leoseo
 */
public class CartProductDTO {
    
    private String productid;
    private String productname;
    private String description;
    private double unitprice;
    private int qtyonhand;
    private int thresholdqty;
    private String memo;
    private boolean active;
    
    public CartProductDTO() {
    }

    public CartProductDTO(String productid, String productname, String description,
            double unitprice, int qtyonhand, int thresholdqty, String memo, boolean active) {
        this.productid = productid;
        this.productname = productname;
        this.description = description;
        this.unitprice = unitprice;
        this.qtyonhand = qtyonhand;
        this.thresholdqty = thresholdqty;
        this.memo = memo;
        this.active = active;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public int getQtyonhand() {
        return qtyonhand;
    }

    public void setQtyonhand(int qtyonhand) {
        this.qtyonhand = qtyonhand;
    }

    public int getThresholdqty() {
        return thresholdqty;
    }

    public void setThresholdqty(int thresholdqty) {
        this.thresholdqty = thresholdqty;
    }
    
    public String getMemo() {
        return memo;
    }
    
    public void setMemo(String memo) {
        this.memo = memo;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
}
