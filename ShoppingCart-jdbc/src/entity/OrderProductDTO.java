/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;

/**
 *
 * @author leoseo
 */
public class OrderProductDTO  implements Serializable{
    
    private String orderid;
    private String productid;
    private int qtyOrder;

    public OrderProductDTO(String orderid, String productid, int qtyOrder) {
        this.orderid = orderid;
        this.productid = productid;
        this.qtyOrder = qtyOrder;
    }

    public OrderProductDTO() {
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public int getQtyOrder() {
        return qtyOrder;
    }

    public void setQtyOrder(int qtyOrder) {
        this.qtyOrder = qtyOrder;
    }
    
    
}
