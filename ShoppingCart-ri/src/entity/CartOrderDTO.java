/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.math.BigDecimal;
import java.util.List;

/**
 * For customer's orders
 * @author leoseo
 */
public class CartOrderDTO {
    
    private String orderid;
    private String customerName;
    private String customerEmail;
    private String customerPhone;
    private String customerAddress;
    private BigDecimal totalCost;
    private List<ProductOrderDTO> productOrders;
    private boolean processed;

    public CartOrderDTO() {
    }

    public CartOrderDTO(String customerName,
            String customerEmail, String customerPhone,
            String customerAddress, BigDecimal totalCost,
            List<ProductOrderDTO> productOrders, boolean processed) {
        this.customerName = customerName;
        this.customerEmail = customerEmail;
        this.customerPhone = customerPhone;
        this.customerAddress = customerAddress;
        this.totalCost = totalCost;
        this.productOrders = productOrders;
        this.processed = processed;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public List<ProductOrderDTO> getProductOrders() {
        return productOrders;
    }

    public void setProductOrders(List<ProductOrderDTO> productOrders) {
        this.productOrders = productOrders;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    @Override
    public String toString() {
        return this == null ? null 
                : "CartOrderDTO{" + "orderid=" + orderid 
                + ", customerName=" + customerName 
                + ", customerEmail=" + customerEmail 
                + ", customerPhone=" + customerPhone 
                + ", customerAddress=" + customerAddress 
                + ", totalCost=" + totalCost 
                + ", productOrders=" + productOrders 
                + ", processed=" + processed + '}';
    }
    
    
    
}
