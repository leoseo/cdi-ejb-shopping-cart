/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

/**
 *
 * @author leoseo
 */
public class ProductOrderDTO {
    
    private CartProductDTO cartProductDTO;
    private Integer qtyOrder;

    public ProductOrderDTO(CartProductDTO cartProductDTO) {
        this.cartProductDTO = cartProductDTO;
        this.qtyOrder = 0;
    }

    public ProductOrderDTO() {
    }

    public CartProductDTO getCartProductDTO() {
        return cartProductDTO;
    }

    public void setCartProductDTO(CartProductDTO cartProductDTO) {
        this.cartProductDTO = cartProductDTO;
    }

    public Integer getQtyOrder() {
        return qtyOrder;
    }

    public void setQtyOrder(Integer qtyOrder) {
        this.qtyOrder = qtyOrder;
    }
    
    public void addQtytoorderByOne() {
        this.qtyOrder++;
    }

    @Override
    public String toString() {
        return this == null ? null : "ProductOrderDTO{" 
                + "cartProductDTO=" + cartProductDTO.toString()
                + ", qtyOrder=" + qtyOrder + '}';
    }
}
