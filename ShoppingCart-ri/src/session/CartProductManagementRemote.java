/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CartProductDTO;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author leoseo
 */
@Remote
public interface CartProductManagementRemote {
    
    public CartProductDTO find(String id);
    
    public boolean hasProduct(String id);
    
    public boolean addProduct(CartProductDTO product);
    
    public boolean updateProduct(CartProductDTO product);

    public boolean deleteProduct(String id);
    
    public boolean enableProduct(String id);
    
    public List<CartProductDTO> getAllProducts();
    
}
