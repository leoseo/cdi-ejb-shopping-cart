/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.EmployeeDTO;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author leoseo
 */
@Remote
public interface EmployeeManagementRemote {
    
    public boolean isValidAndActive(String empid, String password);
    
    public boolean hasEmployee(String empid);
    
    public boolean hasEmail(String email, String empid);
    
    public boolean hasPhone(String phone, String empid);

    public boolean addEmployee(EmployeeDTO emsEmployeeDTO);

    public boolean updateEmployeeDetails(EmployeeDTO emsEmployeeDTO);

    public boolean updateEmployeePassword(String empid, String newPassword);
    
    public boolean updateEmployeePassword2(String oldPassword, String newPassword);

    public EmployeeDTO getEmployeeDetails(String empid);

    public EmployeeDTO getEmployeeDetails2();

    public boolean deleteEmployee(String empid);

    public boolean enableEmployee(String empid);
    
    public List<EmployeeDTO> getEmployeeList();
}
