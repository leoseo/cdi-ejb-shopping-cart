/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CartOrderDTO;
import entity.ProductOrderDTO;
import java.util.Map;
import javax.ejb.Remote;

/**
 *
 * @author leoseo
 */
@Remote
public interface ShoppingCartFacadeRemote {
    
    Map<String, ProductOrderDTO> getShopCart();

    boolean addCartItem(ProductOrderDTO cartItem);
    
    boolean deleteCartItem(String itemId);
    
    boolean updateCartItem(ProductOrderDTO cartItem);
    
    boolean resetShopCart();
    
    boolean placeOrder(CartOrderDTO order);
    
}
