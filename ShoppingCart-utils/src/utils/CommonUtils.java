/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.util.Collection;

/**
 *
 * @author leoseo
 */
public class CommonUtils {
    
    public static boolean isBlank(String input) {
        return input == null || input.trim().isEmpty();
    }
    
    public static boolean isEmpty(Collection<?> col) {
        return col == null || col.isEmpty();
    }
    
    public static int parseIntSilently (String input) {
        try {
            return Integer.parseInt(input);
        } catch (Exception e) {
            return 0;
        }
    } 
    
}
