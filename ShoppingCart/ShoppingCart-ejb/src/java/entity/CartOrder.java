/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author leoseo
 */
@Entity
@Table(name = "CART_ORDER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CartOrder.findAll", query = "SELECT c FROM CartOrder c"),
    @NamedQuery(name = "CartOrder.findByOrderid", query = "SELECT c FROM CartOrder c WHERE c.orderid = :orderid"),
    @NamedQuery(name = "CartOrder.findByCustomername", query = "SELECT c FROM CartOrder c WHERE c.customername = :customername"),
    @NamedQuery(name = "CartOrder.findByCustomeremail", query = "SELECT c FROM CartOrder c WHERE c.customeremail = :customeremail"),
    @NamedQuery(name = "CartOrder.findByCustomerphone", query = "SELECT c FROM CartOrder c WHERE c.customerphone = :customerphone"),
    @NamedQuery(name = "CartOrder.findByCustomeraddress", query = "SELECT c FROM CartOrder c WHERE c.customeraddress = :customeraddress"),
    @NamedQuery(name = "CartOrder.findByTotalcost", query = "SELECT c FROM CartOrder c WHERE c.totalcost = :totalcost"),
    @NamedQuery(name = "CartOrder.findByProcessed", query = "SELECT c FROM CartOrder c WHERE c.processed = :processed"),
    @NamedQuery(name = "CartOrder.countAll", query = "SELECT COUNT(c) FROM CartOrder c")
})
public class CartOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "ORDERID")
    private String orderid;
    @Size(max = 50)
    @Column(name = "CUSTOMERNAME")
    private String customername;
    @Size(max = 54)
    @Column(name = "CUSTOMEREMAIL")
    private String customeremail;
    @Size(max = 10)
    @Column(name = "CUSTOMERPHONE")
    private String customerphone;
    @Size(max = 50)
    @Column(name = "CUSTOMERADDRESS")
    private String customeraddress;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TOTALCOST")
    private BigDecimal totalcost;
    @Column(name = "PROCESSED")
    private Boolean processed;

    public CartOrder() {
    }

    public CartOrder(String orderid, String customername, String customeremail, String customerphone, String customeraddress, BigDecimal totalcost, Boolean processed) {
        this.orderid = orderid;
        this.customername = customername;
        this.customeremail = customeremail;
        this.customerphone = customerphone;
        this.customeraddress = customeraddress;
        this.totalcost = totalcost;
        this.processed = processed;
    }

    public CartOrder(String orderid) {
        this.orderid = orderid;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getCustomeremail() {
        return customeremail;
    }

    public void setCustomeremail(String customeremail) {
        this.customeremail = customeremail;
    }

    public String getCustomerphone() {
        return customerphone;
    }

    public void setCustomerphone(String customerphone) {
        this.customerphone = customerphone;
    }

    public String getCustomeraddress() {
        return customeraddress;
    }

    public void setCustomeraddress(String customeraddress) {
        this.customeraddress = customeraddress;
    }

    public BigDecimal getTotalcost() {
        return totalcost;
    }

    public void setTotalcost(BigDecimal totalcost) {
        this.totalcost = totalcost;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orderid != null ? orderid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CartOrder)) {
            return false;
        }
        CartOrder other = (CartOrder) object;
        if ((this.orderid == null && other.orderid != null) || (this.orderid != null && !this.orderid.equals(other.orderid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CartOrder[ orderid=" + orderid + " ]";
    }

}
