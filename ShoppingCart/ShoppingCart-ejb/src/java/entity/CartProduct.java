/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author leoseo
 */
@Entity
@Table(name = "CART_PRODUCT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CartProduct.findAll", query = "SELECT c FROM CartProduct c"),
    @NamedQuery(name = "CartProduct.findByProductid", query = "SELECT c FROM CartProduct c WHERE c.productid = :productid"),
    @NamedQuery(name = "CartProduct.findByProductname", query = "SELECT c FROM CartProduct c WHERE c.productname = :productname"),
    @NamedQuery(name = "CartProduct.findByDescription", query = "SELECT c FROM CartProduct c WHERE c.description = :description"),
    @NamedQuery(name = "CartProduct.findByUnitprice", query = "SELECT c FROM CartProduct c WHERE c.unitprice = :unitprice"),
    @NamedQuery(name = "CartProduct.findByQtyonhand", query = "SELECT c FROM CartProduct c WHERE c.qtyonhand = :qtyonhand"),
    @NamedQuery(name = "CartProduct.findByThreshold", query = "SELECT c FROM CartProduct c WHERE c.threshold = :threshold"),
    @NamedQuery(name = "CartProduct.findByMemo", query = "SELECT c FROM CartProduct c WHERE c.memo = :memo"),
    @NamedQuery(name = "CartProduct.findByActive", query = "SELECT c FROM CartProduct c WHERE c.active = :active")})
public class CartProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 6)
    @Column(name = "PRODUCTID")
    private String productid;
    @Size(max = 50)
    @Column(name = "PRODUCTNAME")
    private String productname;
    @Size(max = 100)
    @Column(name = "DESCRIPTION")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "UNITPRICE")
    private BigDecimal unitprice;
    @Column(name = "QTYONHAND")
    private Integer qtyonhand;
    @Column(name = "THRESHOLD")
    private Integer threshold;
    @Size(max = 255)
    @Column(name = "MEMO")
    private String memo;
    @Column(name = "ACTIVE")
    private Boolean active;

    public CartProduct() {
    }

    public CartProduct(String productid, String productname, String description, BigDecimal unitprice, Integer qtyonhand, Integer threshold, String memo, Boolean active) {
        this.productid = productid;
        this.productname = productname;
        this.description = description;
        this.unitprice = unitprice;
        this.qtyonhand = qtyonhand;
        this.threshold = threshold;
        this.memo = memo;
        this.active = active;
    }

    public CartProduct(String productid) {
        this.productid = productid;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(BigDecimal unitprice) {
        this.unitprice = unitprice;
    }

    public Integer getQtyonhand() {
        return qtyonhand;
    }

    public void setQtyonhand(Integer qtyonhand) {
        this.qtyonhand = qtyonhand;
    }

    public Integer getThreshold() {
        return threshold;
    }

    public void setThreshold(Integer threshold) {
        this.threshold = threshold;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (productid != null ? productid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CartProduct)) {
            return false;
        }
        CartProduct other = (CartProduct) object;
        if ((this.productid == null && other.productid != null) || (this.productid != null && !this.productid.equals(other.productid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CartProduct[ productid=" + productid + " ]";
    }
    
}
