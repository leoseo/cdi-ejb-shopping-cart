/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author leoseo
 */
@Entity
@Table(name = "CART_PRODUCT_ORDER")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CartProductOrder.findAll", query = "SELECT c FROM CartProductOrder c"),
    @NamedQuery(name = "CartProductOrder.findByOrderid", query = "SELECT c FROM CartProductOrder c WHERE c.cartProductOrderPK.orderid = :orderid"),
    @NamedQuery(name = "CartProductOrder.findByProductid", query = "SELECT c FROM CartProductOrder c WHERE c.cartProductOrderPK.productid = :productid"),
    @NamedQuery(name = "CartProductOrder.findByQtyorder", query = "SELECT c FROM CartProductOrder c WHERE c.qtyorder = :qtyorder")})
public class CartProductOrder implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CartProductOrderPK cartProductOrderPK;
    @Column(name = "QTYORDER")
    private Integer qtyorder;

    public CartProductOrder() {
    }

    public CartProductOrder(CartProductOrderPK cartProductOrderPK, Integer qtyorder) {
        this.cartProductOrderPK = cartProductOrderPK;
        this.qtyorder = qtyorder;
    }

    public CartProductOrder(String orderid, String productid, Integer qtyorder) {
        this.cartProductOrderPK = new CartProductOrderPK(orderid, productid);
        this.qtyorder = qtyorder;
    }

    public CartProductOrderPK getCartProductOrderPK() {
        return cartProductOrderPK;
    }

    public void setCartProductOrderPK(CartProductOrderPK cartProductOrderPK) {
        this.cartProductOrderPK = cartProductOrderPK;
    }

    public Integer getQtyorder() {
        return qtyorder;
    }

    public void setQtyorder(Integer qtyorder) {
        this.qtyorder = qtyorder;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cartProductOrderPK != null ? cartProductOrderPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CartProductOrder)) {
            return false;
        }
        CartProductOrder other = (CartProductOrder) object;
        if ((this.cartProductOrderPK == null && other.cartProductOrderPK != null) || (this.cartProductOrderPK != null && !this.cartProductOrderPK.equals(other.cartProductOrderPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.CartProductOrder[ cartProductOrderPK=" + cartProductOrderPK + " ]";
    }
    
}
