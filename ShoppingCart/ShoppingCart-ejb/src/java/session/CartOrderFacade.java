/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CartOrder;
import entity.CartProductOrder;
import entity.CartProductOrderPK;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author leoseo
 */
@Stateless
public class CartOrderFacade implements CartOrderFacadeLocal {

    private static final Logger LOG = Logger.getLogger(CartOrderFacade.class.getName());

    @PersistenceContext(unitName = "ShoppingCart-ejbPU")
    private EntityManager em;

    @EJB
    CartProductFacadeLocal cartProductFacade;

    public CartOrderFacade() {
    }

    private void create(CartOrder entity) {
        em.persist(entity);
    }

    private void edit(CartOrder entity) {
        em.merge(entity);
    }

    private void remove(CartOrder entity) {
        em.remove(em.merge(entity));
    }

    private void create(CartProductOrder entity) {
        em.persist(entity);
    }

    private void edit(CartProductOrder entity) {
        em.merge(entity);
    }

    private void remove(CartProductOrder entity) {
        em.remove(em.merge(entity));
    }

    @Override
    public boolean addOrder(CartOrder order, Map<CartProductOrderPK, Integer> productOrder) {
        try {
            
            LOG.log(Level.INFO, "Adding main entity ...");
            create(order);
            
            LOG.log(Level.INFO, "Adding weak entities ...");
            productOrder.forEach((k, v) -> create(new CartProductOrder(k, v)));
            
            return true;
        } catch (Exception e) {
            // force rollback in pair
            throw new RuntimeException("Error while inserting order for " + order.getCustomername());
        }
    }

    @Override
    public Long countAll() {
        Query query = em.createNamedQuery("CartOrder.countAll");
        Long count = (Long) query.getSingleResult();
        return count;
    }

}
