/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CartOrder;
import entity.CartProductOrderPK;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author leoseo
 */
@Local
public interface CartOrderFacadeLocal {
    
    public boolean addOrder(CartOrder order, Map<CartProductOrderPK, Integer> productOrder);
    
    public Long countAll();
    
}
