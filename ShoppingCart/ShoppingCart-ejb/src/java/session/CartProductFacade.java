/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CartProduct;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import utils.CommonUtils;

/**
 *
 * @author leoseo
 */
@Stateless
public class CartProductFacade implements CartProductFacadeLocal {

    private static final Logger LOG = Logger.getLogger(CartProductFacade.class.getName());
    
    @PersistenceContext(unitName = "ShoppingCart-ejbPU")
    private EntityManager em;

    public CartProductFacade() {
    }

    private void create(CartProduct entity) {
        em.persist(entity);
    }

    private void edit(CartProduct entity) {
        em.merge(entity);
    }

    private void remove(CartProduct entity) {
        em.remove(em.merge(entity));
    }
    

    @Override
    public CartProduct find(String id) {
        return !CommonUtils.isBlank(id) 
                ? em.find(CartProduct.class, id) : null;
    }

    @Override
    public boolean hasProduct(String id) {
        return find(id) != null;
    }

    @Override
    public boolean addProduct(CartProduct product) {
        if (product == null || hasProduct(product.getProductid())) {
            return false;
        }
        
        create(product);
        return true;
    }

    @Override
    public boolean updateProduct(CartProduct product) {
        if (product == null || !hasProduct(product.getProductid())) {
            return false;
        }
        
        edit(product);
        return true;
    }

    @Override
    public boolean deleteProduct(String id) {
        
        CartProduct product = find(id);
        if (product == null || !product.getActive()) {
            return false;
        }
        
        product.setActive(false);
        return true;
    }

    @Override
    public boolean enableProduct(String id) {
        CartProduct product = find(id);
        if (product == null || product.getActive()) {
            return false;
        }
        
        product.setActive(true);
        return true;
    }

    @Override
    public List<CartProduct> getAllProducts() {
        Query query = em.createNamedQuery("CartProduct.findAll");
        List<CartProduct> results = query.getResultList();
        return results;
    }

}
