/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CartProduct;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author leoseo
 */
@Local
public interface CartProductFacadeLocal {
    
    public CartProduct find(String id);
    
    public boolean hasProduct(String id);
    
    public boolean addProduct(CartProduct product);
    
    public boolean updateProduct(CartProduct product);

    public boolean deleteProduct(String id);
    
    public boolean enableProduct(String id);
    
    public List<CartProduct> getAllProducts();
    
}
