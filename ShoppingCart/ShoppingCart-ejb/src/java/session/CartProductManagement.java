/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CartProduct;
import entity.CartProductDTO;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import utils.CommonUtils;

/**
 *
 * @author leoseo
 */
@Stateless
@DeclareRoles({"ROLE_ADMINISTRATOR", "ROLE_USER"})
public class CartProductManagement implements CartProductManagementRemote {

    private static final Logger LOG = Logger.getLogger(CartProductManagement.class.getName());

    @EJB
    CartProductFacadeLocal cartProductFacade;

    @Override
    @RolesAllowed({"ROLE_USER"})
    public CartProductDTO find(String id) {
        LOG.log(Level.INFO, "Finding product ID {0}", id);
        if (CommonUtils.isBlank(id)) {
            return null;
        }

        CartProduct result = cartProductFacade.find(id);
        return entityToDTO(result);
    }

    @Override
    @RolesAllowed({"ROLE_USER"})
    public boolean hasProduct(String id) {
        LOG.log(Level.INFO, "Checking product ID {0} existance", id);
        return find(id) != null;
    }

    @Override
    @RolesAllowed({"ROLE_USER"})
    public boolean addProduct(CartProductDTO product) {
        LOG.log(Level.INFO, "Adding product {0} existance", product);
        if (product == null || hasProduct(product.getProductid())) {
            return false;
        }
        
        return cartProductFacade.addProduct(dtoToEntity(product));
    }

    @Override
    @RolesAllowed({"ROLE_USER"})
    public boolean updateProduct(CartProductDTO product) {
        LOG.log(Level.INFO, "Updating product {0}", product);
        if (product == null || !hasProduct(product.getProductid())) {
            return false;
        }
        
        return cartProductFacade.updateProduct(dtoToEntity(product));
    }

    @Override
    @RolesAllowed({"ROLE_USER"})
    public boolean deleteProduct(String id) {
        LOG.log(Level.INFO, "Deleting product ID {0}", id);
        if (!hasProduct(id)) {
            return false;
        }
        
        return cartProductFacade.deleteProduct(id);
    }

    @Override
    @RolesAllowed({"ROLE_USER"})
    public boolean enableProduct(String id) {
        LOG.log(Level.INFO, "Enabling product ID {0}", id);
        if (!hasProduct(id)) {
            return false;
        }
        
        return cartProductFacade.enableProduct(id);
    }

    @Override
    @PermitAll
    public List<CartProductDTO> getAllProducts() {
        LOG.log(Level.INFO, "Getting all products");
        
        List<CartProduct> results = cartProductFacade.getAllProducts();
        return results != null && !results.isEmpty() 
                ? results.stream().map(x -> entityToDTO(x)).collect(Collectors.toList())
                : null;
    }

    private CartProductDTO entityToDTO(CartProduct entity) {
        return entity != null
                ? new CartProductDTO(entity.getProductid(), entity.getProductname(),
                        entity.getDescription(), entity.getUnitprice().doubleValue(),
                        entity.getQtyonhand(), entity.getThreshold(), entity.getMemo(),
                        entity.getActive())
                : null;

    }

    private CartProduct dtoToEntity(CartProductDTO dto) {
        return dto != null
                ? new CartProduct(dto.getProductid(), dto.getProductname(),
                        dto.getDescription(), BigDecimal.valueOf(dto.getUnitprice()),
                        dto.getQtyonhand(), dto.getThresholdqty(), dto.getMemo(),
                        dto.isActive())
                : null;
    }
}
