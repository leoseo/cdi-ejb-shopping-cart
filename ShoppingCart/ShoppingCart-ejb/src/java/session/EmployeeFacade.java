/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Employee;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import utils.CommonUtils;

/**
 *
 * @author leoseo
 */
@Stateless
public class EmployeeFacade implements EmployeeFacadeLocal {

    private static final Logger LOG = Logger.getLogger(EmployeeFacade.class.getName());

    @PersistenceContext(unitName = "ShoppingCart-ejbPU")
    private EntityManager em;

    public EmployeeFacade() {
    }

    private void create(Employee entity) {
        em.persist(entity);
    }

    private void edit(Employee entity) {
        em.merge(entity);
    }

    private void remove(Employee entity) {
        em.remove(em.merge(entity));
    }

    @Override
    public Employee find(String empid) {
        return em.find(Employee.class, empid);
    }

    @Override
    public boolean hasEmployee(String empId) {
        return (find(empId) != null);
    }

    @Override
    public boolean addEmployee(Employee employee) {
        Employee e = find(employee.getEmpid());

        if (e != null) {
            return false;
        }

        create(employee);

        return true;
    }

    @Override
    public boolean updateEmployeeDetails(Employee employee) {
        Employee e = find(employee.getEmpid());

        // check again - just to play it safe
        if (e == null) {
            return false;
        }

        // no need to update the primary key - empId
        edit(employee);
        return true;
    }

    @Override
    public boolean updatePassword(String empId, String password, String memo) {
        Employee e = find(empId);

        // check again - just to play it safe
        if (e == null) {
            return false;
        }

        // only need to update the password field
        e.setPassword(password);
        e.setMemo(memo);
        return true;
    }

    @Override
    public boolean deleteEmployee(String empId) {
        Employee e = find(empId);

        // check again - just to play it safe
        if (e == null) {
            return false;
        }

        if (e.getActive() == null) {
            return false;
        }

        if (!e.getActive()) {
            // employee not active already
            return false;
        }

        e.setActive(false);
        return true;
    }

    @Override
    public boolean enableEmployee(String empId) {
        Employee e = find(empId);

        // check again - just to play it safe
        if (e == null) {
            return false;
        }

        if (e.getActive() == null) {
            return false;
        }

        if (e.getActive()) {
            // employee active already
            return false;
        }

        e.setActive(true);
        return true;
    }

    @Override
    public boolean removeEmployee(String empId) {
        Employee e = find(empId);

        // check again - just to play it safe
        if (e == null) {
            return false;
        }

        em.remove(e);
        return true;
    }

    @Override
    public List<Employee> getEmployeeList() {
        try {

            // there are mutiple ways to do this (CriteriaBuilder, JPQL etc.)
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
            Root<Employee> root = cq.from(Employee.class);
            // cq.where(cb.equal(root.get("appgroup"), appgroup));
            cq.orderBy(cb.desc(root.get("active")));
            List<Employee> results = em.createQuery(cq).getResultList();
            return results;

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Errors while getting employee list");
        }
        return null;
    }

    @Override
    public boolean hasEmail(String email, String empid) {
        try {
            if (!CommonUtils.isBlank(email)) {
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
                Root<Employee> root = cq.from(Employee.class);
                List<Predicate> predicates = new ArrayList<>();
                
                // exclude empid
                predicates.add(cb.notEqual(root.get("empid"), empid));

                // add email filter
                LOG.log(Level.INFO, "Adding check for {0}", email);
                predicates.add(cb.equal(root.get("email"), email));
                
                cq.where(predicates.toArray(new Predicate[0]));
                List<Employee> results = em.createQuery(cq).getResultList();
                boolean flag = results != null && !results.isEmpty();
                LOG.log(Level.INFO, "Flag {0} " + (flag ? results.size() : "0"),
                        String.valueOf(flag));
                return flag;
            }

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Errors while checking email {0}", e);
        }
        return false;
    }
    
    @Override
    public boolean hasPhone(String phone, String empid) {
        try {
            if (!CommonUtils.isBlank(phone)) {
                CriteriaBuilder cb = em.getCriteriaBuilder();
                CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
                Root<Employee> root = cq.from(Employee.class);
                List<Predicate> predicates = new ArrayList<>();
                
                // exclude empid
                predicates.add(cb.notEqual(root.get("empid"), empid));

                // add email filter
                LOG.log(Level.INFO, "Adding check for {0}", phone);
                predicates.add(cb.equal(root.get("phone"), phone));
                
                cq.where(predicates.toArray(new Predicate[0]));
                List<Employee> results = em.createQuery(cq).getResultList();
                boolean flag = results != null && !results.isEmpty();
                LOG.log(Level.INFO, "Flag {0} " + (flag ? results.size() : "0"),
                        String.valueOf(flag));
                return flag;
            }

        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Errors while checking phone {0}", e);
        }
        return false;
    }

}
