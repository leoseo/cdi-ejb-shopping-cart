/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Employee;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author leoseo
 */
@Local
public interface EmployeeFacadeLocal {
    
    public Employee find(String empid);
    
    public boolean hasEmployee(String empId);
    
    public boolean hasEmail(String email, String empId);
    
    public boolean hasPhone(String phone, String empId);
    
    public boolean addEmployee(Employee employee);
    
    public boolean updateEmployeeDetails(Employee employee);
    
    public boolean updatePassword(String empId, String password, String memo);

    public boolean deleteEmployee(String empId);
    
    public boolean enableEmployee(String empId);
    
    public boolean removeEmployee(String empId);
    
    public List<Employee> getEmployeeList();
    
}
