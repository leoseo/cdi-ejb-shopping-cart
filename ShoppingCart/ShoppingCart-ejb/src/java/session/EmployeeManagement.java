/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Employee;
import entity.EmployeeDTO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.annotation.Resource;
import javax.annotation.security.DeclareRoles;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.EJB;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;

import utils.CommonUtils;
import utils.HashUtils;

/**
 *
 * @author leoseo
 */
@Stateless
@DeclareRoles({"ROLE_ADMINISTRATOR", "ROLE_USER"})
public class EmployeeManagement implements EmployeeManagementRemote {

    private static final Logger LOG = Logger.getLogger(EmployeeManagement.class.getName());

    @Resource
    SessionContext context;

    @EJB
    EmployeeFacadeLocal employeeFacade;

    @Override
    @PermitAll
    public boolean isValidAndActive(String empid, String password) {
        LOG.log(Level.INFO, "Entered isValidAndActive with {0} and {1}",
                new String[]{empid, password});
        Employee ems = employeeFacade.find(empid);
        return !(ems == null || !ems.getActive() || !ems.getPassword().equals(password));
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public boolean hasEmployee(String empid) {
        LOG.log(Level.INFO, "Entered hasEmployee with {0}", empid);
        if (CommonUtils.isBlank(empid)) {
            return false;
        }
        return employeeFacade.hasEmployee(empid);
    }
    
    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public boolean hasEmail(String email, String empid) {
        LOG.log(Level.INFO, "Entered hasEmail with {0} and {1}",
                new String[] {email, empid});
        if (CommonUtils.isBlank(email)|| CommonUtils.isBlank(empid)) {
            return true;
        }
        return employeeFacade.hasEmail(email, empid);
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public boolean hasPhone(String phone, String empid) {
        LOG.log(Level.INFO, "Entered hasPhone with {0} and {1}",
                new String[] {phone, empid});
        if (CommonUtils.isBlank(phone)|| CommonUtils.isBlank(empid)) {
            return true;
        }
        return employeeFacade.hasPhone(phone, empid);
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public boolean addEmployee(EmployeeDTO employeeDTO) {
        LOG.log(Level.INFO, "Entered addEmployee with {0}", employeeDTO);
        if (employeeDTO == null || hasEmployee(employeeDTO.getEmpid())) {
            return false;
        }
        return employeeFacade.addEmployee(dtoToEntity(employeeDTO));
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public boolean updateEmployeeDetails(EmployeeDTO employeeDTO) {

        LOG.log(Level.INFO, "Entered updateEmployeeDetails with {0}", employeeDTO);
        EmployeeDTO tmp = null;
        if (employeeDTO == null 
                || (tmp = getEmployeeDetails(employeeDTO.getEmpid())) == null) {
            return false;
        }
        employeeDTO.setPassword(tmp.getPassword());
        employeeDTO.setMemo(tmp.getMemo());
        return employeeFacade.updateEmployeeDetails(dtoToEntity(employeeDTO));
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public boolean updateEmployeePassword(String empid, String newPassword) {
        LOG.log(Level.INFO, "Entered updateEmployeePassword with {0}", empid);
        EmployeeDTO employeeDTO = null;
        if (CommonUtils.isBlank(empid)
                || (employeeDTO = getEmployeeDetails(empid)) == null) {
            return false;
        }

        employeeDTO.setPassword(HashUtils.getSHA256HashedString(newPassword));
        employeeDTO.setMemo(newPassword);
        return employeeFacade.updateEmployeeDetails(dtoToEntity(employeeDTO));
    }

    @Override
    @RolesAllowed({"ROLE_USER"})
    public boolean updateEmployeePassword2(String oldPassword, String newPassword) {
        String empid = context.getCallerPrincipal().getName();
        LOG.log(Level.INFO, "Entered updateEmployeePassword2 for {0}", empid);
        EmployeeDTO employeeDTO = null;
        String hashedNewPassword = HashUtils.getSHA256HashedString(newPassword);
        if (CommonUtils.isBlank(empid)
                || (employeeDTO = getEmployeeDetails(empid)) == null
                || employeeDTO.getPassword().equals(hashedNewPassword)) {
            return false;
        }

        employeeDTO.setPassword(hashedNewPassword);
        employeeDTO.setMemo(newPassword);
        return employeeFacade.updateEmployeeDetails(dtoToEntity(employeeDTO));
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public EmployeeDTO getEmployeeDetails(String empid) {
        LOG.log(Level.INFO, "Entered getEmployeeDetails for {0}", empid);
        Employee emp = employeeFacade.find(empid);
        if (emp == null) {
            return null;
        }

        LOG.log(Level.INFO, "Found {0}. Converting ...", empid);
        return entityToDTO(emp);
    }

    @Override
    @RolesAllowed({"ROLE_USER"})
    public EmployeeDTO getEmployeeDetails2() {
        String empid = context.getCallerPrincipal().getName();
        LOG.log(Level.INFO, "Entered getEmployeeDetails2 for {0}", empid);
        return this.getEmployeeDetails(empid);
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public boolean deleteEmployee(String empid) {
        LOG.log(Level.INFO, "Entered deleteEmployee for {0}", empid);
        if (empid == null || !hasEmployee(empid)) {
            return false;
        }
        return employeeFacade.deleteEmployee(empid);
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public boolean enableEmployee(String empid) {
        LOG.log(Level.INFO, "Entered enableEmployee for {0}", empid);
        if (empid == null || !hasEmployee(empid)) {
            return false;
        }
        return employeeFacade.enableEmployee(empid);
    }

    @Override
    @RolesAllowed({"ROLE_ADMINISTRATOR"})
    public List<EmployeeDTO> getEmployeeList() {
        List<Employee> results = employeeFacade.getEmployeeList();
        if (results == null || results.isEmpty()) {
            return null;
        }
        return results.stream().map(item -> entityToDTO(item)).collect(Collectors.toList());
    }

    private Employee dtoToEntity(EmployeeDTO dto) {
        return dto != null
                ? new Employee(dto.getEmpid(), dto.getName(),
                        dto.getPassword(), dto.getEmail(), dto.getPhone(),
                        dto.getAddress(), dto.getAppgroup(), dto.isActive(), dto.getMemo())
                : null;
    }

    private EmployeeDTO entityToDTO(Employee entity) {
        return entity != null
                ? new EmployeeDTO(entity.getEmpid(), entity.getName(),
                        entity.getPassword(), entity.getEmail(), entity.getPhone(),
                        entity.getAddress(), entity.getAppgroup(), entity.getActive(),
                        entity.getMemo())
                : null;

    }

}
