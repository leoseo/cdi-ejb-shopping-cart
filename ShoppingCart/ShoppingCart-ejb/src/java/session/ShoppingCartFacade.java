/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.CartOrder;
import entity.CartOrderDTO;
import entity.CartProduct;
import entity.CartProductDTO;
import entity.CartProductOrderPK;
import entity.ProductOrderDTO;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateful;
import utils.CommonUtils;

/**
 *
 * @author leoseo
 */
@Stateful
@PermitAll
public class ShoppingCartFacade implements ShoppingCartFacadeRemote {

    private static final Logger LOG = Logger.getLogger(ShoppingCartFacade.class.getName());

    @EJB
    CartOrderFacadeLocal cartOrderFacade;

    @EJB
    CartProductFacadeLocal cartProductFacade;

    private Map<String, ProductOrderDTO> shopCart = null;

    public ShoppingCartFacade() {
        if (shopCart == null) {
            shopCart = new LinkedHashMap<String, ProductOrderDTO>();
        } else {
            // should not be here - just check!
            shopCart.clear();
        }
    }

    @Override
    public Map<String, ProductOrderDTO> getShopCart() {
        LOG.log(Level.INFO, "RUN INTO HERE!!");
        return shopCart;
    }

    @Override
    public boolean addCartItem(ProductOrderDTO cartItem) {

        LOG.log(Level.INFO, "Adding cart item [{0}]", cartItem.toString());
        String productid = null;
        try {

            productid = cartItem.getCartProductDTO().getProductid();
            ProductOrderDTO item = shopCart.get(productid);

            if (item == null) {
                LOG.log(Level.INFO, "Item {0} was not added before. Adding...", productid);
                item = cartItem;
                shopCart.put(productid, item);
            }
            
            CartProductDTO productItem = item.getCartProductDTO();
            if (productItem.getQtyonhand() > item.getQtyOrder()
                    && productItem.getThresholdqty() > item.getQtyOrder()) {
                item.addQtytoorderByOne();
                return true;
            }
            
            LOG.log(Level.INFO, "Insufficient quantity !");
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Failed to add item {0} to the cart", productid);
            LOG.log(Level.SEVERE, "Error: {0}", ex.getMessage());
        }

        return false;
    }

    @Override
    public boolean deleteCartItem(String itemId) {
        LOG.log(Level.INFO, "Deleting cart item ID {0}", itemId);
        try {
            return shopCart.remove(itemId) != null;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Failed to delete cart item ID {0}", itemId);
            LOG.log(Level.SEVERE, "Err {0}", e.getMessage());
        }

        return false;
    }

    @Override
    public boolean updateCartItem(ProductOrderDTO updated) {

        LOG.log(Level.INFO, "Updating cart item [{0}]", updated.toString());
        String productid = null;
        try {
            productid = updated.getCartProductDTO().getProductid();
            shopCart.put(productid, updated);
            LOG.log(Level.INFO, "Successfully updated cart item [{0}]", updated.toString());
            return true;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Failed to update cart item ID {0}", productid);
            LOG.log(Level.SEVERE, "Err {0}", e.getMessage());
        }

        return false;

    }

    @Override
    public boolean resetShopCart() {
        LOG.log(Level.INFO, "Deleting all cart items ...");
        try {
            shopCart.clear();
            return true;
        } catch (Exception e) {
            LOG.log(Level.SEVERE, "Failed to delete all cart items ...");
            LOG.log(Level.SEVERE, "Err {0}", e.getMessage());
        }
        return false;
    }

    @Override
    public boolean placeOrder(CartOrderDTO orderDTO) {
        LOG.log(Level.INFO, "Placing order ...", orderDTO);

        List<ProductOrderDTO> orderProducts = null;
        Map<CartProductOrderPK, Integer> pks = new LinkedHashMap<>();
        if (orderDTO != null
                && !CommonUtils.isEmpty(orderProducts = orderDTO.getProductOrders())) {
            
            // counting to set while looping
            Long count = cartOrderFacade.countAll();
            LOG.log(Level.INFO, "finding max count {0}", count);
            String orderID = String.format("%06d", count + 1);
            orderDTO.setOrderid(orderID);
            
            // validate productid
            List<CartProduct> allProducts = cartProductFacade.getAllProducts();
            for (ProductOrderDTO orderProduct : orderProducts) {
                String productid = orderProduct.getCartProductDTO().getProductid();
                LOG.log(Level.INFO, "Checking productID {0}", productid);
                boolean valid = allProducts.stream()
                        .filter(x -> x.getProductid().equals(productid))
                        .findAny().isPresent();
                if (!valid) {
                    throw new RuntimeException("Invalid productid " + productid);
                }
                // create composite primary key
                pks.put(new CartProductOrderPK(orderID,
                        orderProduct.getCartProductDTO().getProductid()),
                        orderProduct.getQtyOrder());
            }

            LOG.log(Level.INFO, "Done checking. proceeding...");
            
            return cartOrderFacade.addOrder(dtoToEntity(orderDTO), pks);
        }

        return false;
    }

    private CartOrder dtoToEntity(CartOrderDTO order) {
        return order != null
                ? new CartOrder(order.getOrderid(), order.getCustomerName(),
                        order.getCustomerEmail(), order.getCustomerPhone(),
                        order.getCustomerAddress(), order.getTotalCost(),
                        order.isProcessed())
                : null;
    }
}
