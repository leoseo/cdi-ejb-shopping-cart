/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constants;

/**
 *
 * @author leoseo
 */
public class Constants {

    public final static String ADMIN = "ROLE_ADMINISTRATOR";
    public final static String STAFF = "ROLE_USER";

    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String EDIT = "edit";
    public static final String DISABLE = "disable";

    public static final String DEBUG = "debug";
    public static final String OK = "success";
    public static final String FAIL = "failure";

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    
    public static final String PHONE_PATTERN = "^(\\d+)$";

}
