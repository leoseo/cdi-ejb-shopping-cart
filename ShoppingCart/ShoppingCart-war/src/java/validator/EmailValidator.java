/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import constants.Constants;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import session.EmployeeManagementRemote;
import utils.CommonUtils;

/**
 *
 * @author leoseo
 */
@FacesValidator("validator.emailValidator")
public class EmailValidator implements Validator {
    
    private Pattern pattern;
    private static final Logger LOG = Logger.getLogger(EmailValidator.class.getName());
    
    public EmailValidator() {
        pattern = Pattern.compile(Constants.EMAIL_PATTERN);
    }

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object val) throws ValidatorException {
        
        String emailInput = (String) val;
        
        LOG.log(Level.INFO, "Validating email : {0}", emailInput);

        if (!CommonUtils.isBlank(emailInput)) {
            Matcher matcher = pattern.matcher(emailInput);
            if (!matcher.matches()) {
                FacesMessage message = new FacesMessage("Invalid email type!");
                throw new ValidatorException(message);
            }
        }

    }

}
