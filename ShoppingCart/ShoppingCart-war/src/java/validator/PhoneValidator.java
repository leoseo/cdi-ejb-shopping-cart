/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package validator;

import constants.Constants;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import utils.CommonUtils;

/**
 *
 * @author leoseo
 */
@FacesValidator("validator.phoneValidator")
public class PhoneValidator implements Validator {

    private Pattern pattern;
    private static final Logger LOG = Logger.getLogger(PhoneValidator.class.getName());

    public PhoneValidator() {
        pattern = Pattern.compile(Constants.PHONE_PATTERN);
    }

    @Override
    public void validate(FacesContext fc, UIComponent uic, Object val) throws ValidatorException {

        String phoneInput = (String) val;
        LOG.log(Level.INFO, "Validating phone : {0}", phoneInput);

        if (!CommonUtils.isBlank(phoneInput)) {

            Matcher matcher = pattern.matcher(phoneInput);
            if (!matcher.matches()) {
                FacesMessage message = new FacesMessage("Phone can only contain numbers!");
                throw new ValidatorException(message);
            }
        }
    }

}
