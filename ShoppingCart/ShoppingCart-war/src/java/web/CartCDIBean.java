/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import entity.CartOrderDTO;
import entity.CartProductDTO;
import entity.ProductOrderDTO;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.ejb.EJB;
import javax.faces.component.UIInput;
import javax.faces.event.ValueChangeEvent;
import session.ShoppingCartFacadeRemote;
import utils.CommonUtils;

/**
 *
 * @author leoseo
 */
@Named(value = "cartCDIBean")
@SessionScoped
public class CartCDIBean implements Serializable {

    @EJB
    ShoppingCartFacadeRemote shoppingCartFacadeRemote;

    private static final Logger LOG = Logger.getLogger(CartCDIBean.class.getName());

    private List<ProductOrderDTO> shopCart;
    private CartProductDTO tmpDto;
    private double cartTotal;
    private String customerName;
    private String customerEmail;
    private String customerPhone;
    private String customerAddress;

    public CartCDIBean() {
    }

    public void addCartItem() {
        LOG.log(Level.INFO, "Adding cart item {0} ...", tmpDto);
        boolean result = false;
        if (tmpDto.getQtyonhand() > 0 && tmpDto.getThresholdqty() > 0) {
            result = shoppingCartFacadeRemote.addCartItem(new ProductOrderDTO(tmpDto));
        } else {
            LOG.log(Level.INFO, "Insufficient quantity!");
        }
        LOG.log(Level.INFO, "Added cart item result {0}", String.valueOf(result));
    }

    public void resetShopCart() {
        LOG.log(Level.INFO, "Resetting shop cart ...");
        boolean result = shoppingCartFacadeRemote.resetShopCart();
        LOG.log(Level.INFO, "Resetted shop cart result {0}..", String.valueOf(result));
    }

    public void updateQtyOrder(ValueChangeEvent e) {

        boolean result = false;
        try {
            Integer qtyOrder = (Integer) e.getNewValue();
            String productid = (String) ((UIInput) e.getSource()).getAttributes().get("productid");
            LOG.log(Level.INFO, "Check before updating quantity order {0}...", qtyOrder);
            LOG.log(Level.INFO, "Check before updating productid {0}...", productid);
            if (qtyOrder != null && qtyOrder > 0 && !CommonUtils.isBlank(productid)
                    && !CommonUtils.isEmpty(shopCart)) {
                ProductOrderDTO product = getCartItemByID(productid);
                int qtyOnHand = product.getCartProductDTO().getQtyonhand();
                int thresholdqty = product.getCartProductDTO().getThresholdqty();
                if (qtyOnHand >= qtyOrder && thresholdqty >= qtyOrder) {
                    LOG.log(Level.INFO, "Begin updating for productID {0}", productid);
                    product.setQtyOrder(qtyOrder);
                    result = shoppingCartFacadeRemote.updateCartItem(product);
                } else {
                    LOG.log(Level.INFO, "Insufficient quantity!");
                }
            }
        } catch (Exception ex) {
            LOG.log(Level.SEVERE, "Error while updating cart {0}", ex);
        }

        LOG.log(Level.INFO, "Updated quantity order {0}...", String.valueOf(result));
    }

    public void deleteCartItem(String itemId) {
        boolean result = shoppingCartFacadeRemote.deleteCartItem(itemId);
        LOG.log(Level.INFO, "Deleted result of item ID [{0}]: {1}",
                new String[]{itemId, String.valueOf(result)});
    }

    public String purchase() {
        LOG.log(Level.INFO, "Place order for customer {0}...", customerName);
        CartOrderDTO order = new CartOrderDTO(customerName, customerEmail, customerPhone,
                customerAddress, BigDecimal.valueOf(cartTotal), shopCart, false);
        boolean result = shoppingCartFacadeRemote.placeOrder(order);
        LOG.log(Level.INFO, "Place order result {0}", String.valueOf(result));

        if (result) {
            resetShopCart();
            return "/customer/addOrderSuccessful.xhtml?faces-redirect=true";
        } else {
            return "/customer/addOrderFailure.xhtml?faces-redirect=true";
        }
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public CartProductDTO getTmpDto() {
        return tmpDto;
    }

    public void setTmpDto(CartProductDTO tmpDto) {
        this.tmpDto = tmpDto;
    }

    public List<ProductOrderDTO> getShopCart() {
        Map<String, ProductOrderDTO> results = shoppingCartFacadeRemote.getShopCart();
        shopCart = results.values().stream().collect(Collectors.toList());
        return shopCart;
    }

    public double getCartTotal() {
        if (shopCart == null || shopCart.isEmpty()) {
            cartTotal = 0.0;
        } else {
            double total = 0.0;
            for (ProductOrderDTO item : shopCart) {
                total = total + item.getQtyOrder().doubleValue() * item.getCartProductDTO().getUnitprice();
            }
            cartTotal = total;
        }

        return cartTotal;
    }

    private ProductOrderDTO getCartItemByID(String productid) throws Exception {
        return shopCart.stream()
                .filter(x -> productid.equals(x.getCartProductDTO().getProductid()))
                .findAny()
                .orElseThrow(() -> new Exception("Invalid productID " + productid));
    }
}
