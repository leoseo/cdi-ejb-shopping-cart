/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import entity.CartProductDTO;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.ejb.EJB;
import session.CartProductManagementRemote;

/**
 *
 * @author leoseo
 */
@Named(value = "catalogCDIBean")
@SessionScoped
public class CatalogCDIBean implements Serializable {
    
    @EJB
    CartProductManagementRemote cartProductManagement;

    private List<CartProductDTO> list;
    
    public CatalogCDIBean() {
    }

    public List<CartProductDTO> getList() {
        if (list == null) {
            list = cartProductManagement.getAllProducts();
        }
        return list;
    }
    
}
