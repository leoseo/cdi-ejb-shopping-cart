/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import session.EmployeeManagementRemote;

import constants.Constants;
import utils.HashUtils;

/**
 *
 * @author leoseo
 */
@Named(value = "loginCDIBean")
@SessionScoped
public class LoginCDIBean implements Serializable {

    @EJB
    private EmployeeManagementRemote employeeManagement;

    private static final Logger LOG = Logger.getLogger(LoginCDIBean.class.getName());

    private String empId;
    private String password;
    
    public LoginCDIBean() {
    }

    public String loginResult() {

        LOG.log(Level.INFO, "Enter login method with {0} and {1}",
                new String[]{empId, password});

        if (empId == null || password == null) {
            return Constants.LOGIN;
        }

        String hashedPassword = HashUtils.getSHA256HashedString(password);
        boolean result = employeeManagement.isValidAndActive(empId, hashedPassword);
        LOG.log(Level.INFO, "Result is {0}", result);
        if (!result) {
            return Constants.LOGIN;
        }

        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) context
                .getExternalContext().getRequest();
        try {
            request.login(empId, password);
        } catch (Exception e) {
            return Constants.LOGIN;
        }

        if (request.isUserInRole(Constants.ADMIN)) {
            return "/admin/mainmenu.xhtml?faces-redirect=true";
        } else {
            return "/user/mainmenu.xhtml?faces-redirect=true";
        }
    }

    public String logoutResult() {
        // terminate the session by invalidating the session context
        FacesContext fc = FacesContext.getCurrentInstance();
        HttpServletRequest request = (HttpServletRequest) fc.getExternalContext().getRequest();
        try {
            request.logout();
        } catch (Exception ex) {
            // do nothing
            LOG.log(Level.SEVERE, "Errors during logout {0}", ex);
        }
        // terminate the session by invalidating the session context
        HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
        session.invalidate();
        // terminate the user's login credentials
        return Constants.LOGOUT;
    }
    
    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
