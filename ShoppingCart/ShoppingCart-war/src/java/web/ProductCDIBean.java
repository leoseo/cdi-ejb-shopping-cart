/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import constants.Constants;
import entity.CartProductDTO;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.inject.Inject;
import session.CartProductManagementRemote;
import utils.CommonUtils;

/**
 *
 * @author leoseo
 */
@Named(value = "productCDIBean")
@SessionScoped
public class ProductCDIBean implements Serializable {

    @EJB
    CartProductManagementRemote cartProductManagement;

    @Inject
    private Conversation conversation;

    private static final Logger LOG = Logger.getLogger(ProductCDIBean.class.getName());
    
    private List<CartProductDTO> list;
    private CartProductDTO tmpDto;
    private String productid;
    private String productname;
    private String description;
    private double unitprice;
    private int qtyonhand;
    private int thresholdqty;
    private String memo;
    private boolean active;

    public ProductCDIBean() {
    }

    public String redirectToDisableView(String productID) {
        LOG.log(Level.INFO, "Disabling Product ID {0}", productID);
        this.productid = productID;
        deleteProduct();
        return "/user/viewEmployeeList.xhtml?faces-redirect=true";
    }

    public String deleteProduct() {

        LOG.log(Level.INFO, "Preparing to delete {0}", productid);
        if (CommonUtils.isBlank(productid)) {
            return Constants.DEBUG;
        }

        conversation.begin();
        boolean result = cartProductManagement.deleteProduct(productid);
        refreshList();

        LOG.log(Level.INFO, "Deleted ID {0} result {1}",
                new String[]{productid, String.valueOf(result)});
        conversation.end();

        return result ? Constants.OK : Constants.FAIL;
    }

    public String redirectToEnableView(String productID) {
        LOG.log(Level.INFO, "Enabling Product ID {0}", productID);
        this.productid = productID;
        enableProduct();
        return "/user/viewEmployeeList.xhtml?faces-redirect=true";
    }

    public String enableProduct() {

        LOG.log(Level.INFO, "Preparing to enable {0}", productid);
        if (CommonUtils.isBlank(productid)) {
            return Constants.DEBUG;
        }

        conversation.begin();
        boolean result = cartProductManagement.enableProduct(productid);
        refreshList();

        LOG.log(Level.INFO, "Enabled ID {0} result {1}",
                new String[]{productid, String.valueOf(result)});
        conversation.end();

        return result ? Constants.OK : Constants.FAIL;
    }

    public String redirectToEditView() {
        LOG.log(Level.INFO, "Redirecting to edit view ...");
        if (tmpDto == null) {
            return Constants.DEBUG;
        }
        setTmpDtoToModel();
        return Constants.EDIT;
    }
    
    public String changeProduct() {
        LOG.log(Level.INFO, "Updating product {0}...", productid);
        if (CommonUtils.isBlank(productid)) {
            return Constants.DEBUG;
        }

        conversation.begin();
        
        CartProductDTO dto = new CartProductDTO(productid, productname,
                description, unitprice, qtyonhand, thresholdqty, memo, active);
       
        boolean result = cartProductManagement.updateProduct(dto);
        LOG.log(Level.INFO, "Updated product {0}...", dto);

        refreshList();
        conversation.end();
        return result ? Constants.OK : Constants.FAIL;
    }
    
    public String addProduct() {
        LOG.log(Level.INFO, "Adding product {0}", productid);
        if (CommonUtils.isBlank(productid)) {
            return Constants.DEBUG;
        }
        
        conversation.begin();
        CartProductDTO dto = new CartProductDTO(productid, productname,
                description, unitprice, qtyonhand, thresholdqty, memo, active);

        boolean result = cartProductManagement.addProduct(dto);
        LOG.log(Level.INFO, "Result {0} - added product {1}",
                new String[] {String.valueOf(result), dto.toString()});

        refreshList();
        conversation.end();
        return result ? Constants.OK : Constants.FAIL;
    }
    
    public String setProductDetails() {

        LOG.log(Level.INFO, "Finding productid {0} to update", productid);
        if (CommonUtils.isBlank(productid) || conversation == null) {
            return Constants.DEBUG;
        }
        conversation.begin();
        tmpDto = cartProductManagement.find(productid);
        if (tmpDto == null) {
            conversation.end();
            return Constants.FAIL;
        }
        setTmpDtoToModel();
        conversation.end();
        return Constants.OK;
    }
    
    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public int getQtyonhand() {
        return qtyonhand;
    }

    public void setQtyonhand(int qtyonhand) {
        this.qtyonhand = qtyonhand;
    }

    public int getThresholdqty() {
        return thresholdqty;
    }

    public void setThresholdqty(int thresholdqty) {
        this.thresholdqty = thresholdqty;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public CartProductDTO getTmpDto() {
        return tmpDto;
    }

    public void setTmpDto(CartProductDTO tmpDto) {
        this.tmpDto = tmpDto;
    }
    
    public List<CartProductDTO> getList() {
        return cartProductManagement.getAllProducts();
    }

    private void refreshList() {
        list = cartProductManagement.getAllProducts();
    }

    private void setTmpDtoToModel() {
        productid = tmpDto.getProductid();
        productname = tmpDto.getProductname();
        description = tmpDto.getDescription();
        unitprice = tmpDto.getUnitprice();
        qtyonhand = tmpDto.getQtyonhand();
        thresholdqty = tmpDto.getThresholdqty();
        memo = tmpDto.getMemo();
        active = tmpDto.isActive();
    }

}
