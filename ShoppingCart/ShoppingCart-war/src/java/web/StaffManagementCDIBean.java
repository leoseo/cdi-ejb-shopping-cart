/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package web;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.inject.Inject;
import session.EmployeeManagementRemote;

import constants.Constants;
import entity.EmployeeDTO;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import utils.CommonUtils;
import utils.HashUtils;

/**
 *
 * @author leoseo
 */
@Named(value = "staffManagementCDIBean")
@SessionScoped
public class StaffManagementCDIBean implements Serializable {

    private static final Logger LOG = Logger.getLogger(StaffManagementCDIBean.class.getName());

    @Inject
    private Conversation conversation;

    @EJB
    private EmployeeManagementRemote employeeManagement;

    private Pattern emailPattern;
    private Pattern phonePattern;
    private List<EmployeeDTO> list = null;
    private EmployeeDTO tmpDto = null;
    private String empid;
    private String name;
    private String password;
    private String cpassword;
    private String newpassword;
    private String phone;
    private String address;
    private String email;
    private String appgroup;
    private Boolean active;
    private String memo;
    
    public StaffManagementCDIBean() {
        emailPattern = Pattern.compile(Constants.EMAIL_PATTERN);
        phonePattern = Pattern.compile(Constants.PHONE_PATTERN);
    }

    public String addEmployee() {
        LOG.log(Level.INFO, "Entered addEmployee()");
        if (CommonUtils.isBlank(empid)) {
            return Constants.DEBUG;
        }
        EmployeeDTO employeeDTO = new EmployeeDTO(empid, name,
                HashUtils.getSHA256HashedString(password),
                email, phone, address, appgroup, active, password);

        startConversation();
        LOG.log(Level.INFO, "Adding employee with {0}", employeeDTO.toString());
        boolean result = employeeManagement.addEmployee(employeeDTO);
        LOG.log(Level.INFO, "Added employee {0}", employeeDTO.getEmpid());

        refreshList();
        endConversation();
        return result ? Constants.OK : Constants.FAIL;
    }

    public String changeEmployee() {

        LOG.log(Level.INFO, "Updating employee {0}...", empid);
        if (CommonUtils.isBlank(empid)) {
            return Constants.DEBUG;
        }

        startConversation();

        EmployeeDTO employeeDTO = new EmployeeDTO(empid, name, null,
                email, phone, address, appgroup, active, null);
        boolean result = employeeManagement.updateEmployeeDetails(employeeDTO);
        LOG.log(Level.INFO, "Updated employee {0}...", empid);

        refreshList();
        endConversation();
        return result ? Constants.OK : Constants.FAIL;
    }

    public String setEmployeeDetails() {

        LOG.log(Level.INFO, "Finding empid {0} to update", empid);
        if (CommonUtils.isBlank(empid) || conversation == null) {
            return Constants.DEBUG;
        }
        startConversation();
        tmpDto = employeeManagement.getEmployeeDetails(empid);
        if (tmpDto == null) {
            endConversation();
            return Constants.FAIL;
        }
        setTmpDtoToModel();
        endConversation();
        return Constants.OK;
    }

    public String redirectToEditView() {
        LOG.log(Level.INFO, "Redirecting to edit view ...");
        if (tmpDto == null) {
            return Constants.DEBUG;
        }
        setTmpDtoToModel();
        return Constants.EDIT;
    }

    public String redirectToDisableView(String disableID) {
        LOG.log(Level.INFO, "Disabling ID {0}", disableID);
        empid = disableID;
        deleteEmployee();
        return "/admin/viewEmployeeList.xhtml?faces-redirect=true";
    }

    public String deleteEmployee() {

        LOG.log(Level.INFO, "Preparing to delete {0}", empid);
        if (CommonUtils.isBlank(empid)) {
            return Constants.DEBUG;
        }

        startConversation();
        boolean result = employeeManagement.deleteEmployee(empid);
        refreshList();

        LOG.log(Level.INFO, "Deleted ID {0} {1}",
                new String[]{empid, String.valueOf(result)});
        endConversation();

        return result ? Constants.OK : Constants.FAIL;
    }

    public String redirectToEnableView(String enabledId) {
        LOG.log(Level.INFO, "Enabling ID {0}", enabledId);
        empid = enabledId;
        enableView();
        return "/admin/viewEmployeeList.xhtml?faces-redirect=true";
    }

    public String enableView() {

        LOG.log(Level.INFO, "Preparing to enable {0}", empid);
        if (CommonUtils.isBlank(empid)) {
            return Constants.DEBUG;
        }

        startConversation();
        boolean result = employeeManagement.enableEmployee(empid);
        refreshList();

        LOG.log(Level.INFO, "Enabled ID {0} {1}",
                new String[]{empid, String.valueOf(result)});
        endConversation();

        return result ? Constants.OK : Constants.FAIL;
    }

    public String changeEmployeePassword() {

        LOG.log(Level.INFO, "Begin updating password ID {0}", empid);
        startConversation();
        if (CommonUtils.isBlank(empid)) {
            endConversation();
            return Constants.DEBUG;
        }
        boolean result = employeeManagement.updateEmployeePassword(empid, newpassword);
        LOG.log(Level.INFO, "Password updated? {0}", String.valueOf(result));
        endConversation();
        return result ? Constants.OK : Constants.FAIL;
    }

    public String displayUser() {
        LOG.log(Level.INFO, "Display info for logged in user...");
        startConversation();
        tmpDto = employeeManagement.getEmployeeDetails2();
        if (tmpDto == null) {
            endConversation();
            return Constants.DEBUG;
        }
        setTmpDtoToModel();
        endConversation();
        return Constants.OK;
    }

    public String changeStaffPassword() {
        LOG.log(Level.INFO, "Begin updating password for logged in user...");
        startConversation();
        boolean result = employeeManagement.updateEmployeePassword2(password, newpassword);
        LOG.log(Level.INFO, "Password updated? {0}", String.valueOf(result));
        endConversation();
        return result ? Constants.OK : Constants.FAIL;
    }

    public void validateNewPassword(FacesContext context,
            UIComponent componentToValidate, Object value)
            throws ValidatorException {

        // get new password
        String oldPwd = (String) value;

        // get old password
        UIInput newPwdComponent = (UIInput) componentToValidate.getAttributes().get("newpwd");
        String newPwd = (String) newPwdComponent.getSubmittedValue();

        if (oldPwd.equals(newPwd)) {
            FacesMessage message = new FacesMessage(
                    "Old Password and New Password are the same! They must be different.");
            throw new ValidatorException(message);
        }

    }

    public void validatePasswordPair(FacesContext context,
            UIComponent componentToValidate,
            Object pwdValue) throws ValidatorException {

        // get password
        String pwd = (String) pwdValue;

        // get confirm password
        UIInput cnfPwdComponent = (UIInput) componentToValidate.getAttributes().get("cnfpwd");
        String cnfPwd = (String) cnfPwdComponent.getSubmittedValue();

        LOG.log(Level.INFO, "password : {0}", pwd);
        LOG.log(Level.INFO, "confirm password : {0}", cnfPwd);

        if (!pwd.equals(cnfPwd)) {
            FacesMessage message = new FacesMessage(
                    "Password and Confirm Password are not the same! They must be the same.");
            throw new ValidatorException(message);
        }
    }

    public void validateNewPasswordPair(FacesContext context,
            UIComponent componentToValidate,
            Object newValue) throws ValidatorException {

        // get new password
        String newPwd = (String) newValue;

        // get confirm password
        UIInput cnfPwdComponent = (UIInput) componentToValidate.getAttributes().get("cnfpwd");
        String cnfPwd = (String) cnfPwdComponent.getSubmittedValue();

        LOG.log(Level.INFO, "new password : {0}", newPwd);
        LOG.log(Level.INFO, "confirm password : {0}", cnfPwd);

        if (!newPwd.equals(cnfPwd)) {
            FacesMessage message = new FacesMessage(
                    "New Password and Confirm New Password are not the same! They must be the same.");
            throw new ValidatorException(message);
        }
    }

    public void validateEmail(FacesContext context,
            UIComponent component,
            Object val) throws ValidatorException {

        String emailInput = (String) val;
        String checkEmpid = (String) component.getAttributes().get("empID");
        
        LOG.log(Level.INFO, "Validating email : {0} for " + checkEmpid, emailInput);

        if (!CommonUtils.isBlank(emailInput)) {
            Matcher matcher = emailPattern.matcher(emailInput);
            if (!matcher.matches()) {
                FacesMessage message = new FacesMessage("Invalid email type!");
                throw new ValidatorException(message);
            }

            if (employeeManagement.hasEmail(emailInput, checkEmpid)) {
                FacesMessage message = new FacesMessage("Email was already taken!");
                throw new ValidatorException(message);
            }
        }
    }

    public void validatePhone(FacesContext context,
            UIComponent component,
            Object val) throws ValidatorException {

        String phoneInput = (String) val;
        String checkEmpid = (String) component.getAttributes().get("empID");
        LOG.log(Level.INFO, "Validating phone : {0} for " + checkEmpid, phoneInput);

        if (!CommonUtils.isBlank(phoneInput)) {

            Matcher matcher = phonePattern.matcher(phoneInput);
            if (!matcher.matches()) {
                FacesMessage message = new FacesMessage("Phone can only contain numbers!");
                throw new ValidatorException(message);
            }

            if (employeeManagement.hasPhone(phoneInput, checkEmpid)) {
                FacesMessage message = new FacesMessage("Phone was already taken!");
                throw new ValidatorException(message);
            }
        }
    }
    
    public String getEmpid() {
        return empid;
    }

    public void setEmpid(String empid) {
        this.empid = empid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCpassword() {
        return cpassword;
    }

    public void setCpassword(String cpassword) {
        this.cpassword = cpassword;
    }

    public String getNewpassword() {
        return newpassword;
    }

    public void setNewpassword(String newpassword) {
        this.newpassword = newpassword;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAppgroup() {
        return appgroup;
    }

    public void setAppgroup(String appgroup) {
        this.appgroup = appgroup;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public void startConversation() {
        conversation.begin();
    }

    public void endConversation() {
        conversation.end();
    }
    
    public EmployeeDTO getTmpDto() {
        return tmpDto;
    }

    public void setTmpDto(EmployeeDTO tmpDto) {
        this.tmpDto = tmpDto;
    }
    
    public List<EmployeeDTO> getList() {
        if (list == null) {
            refreshList();
        }
        return list;
    }

    private void refreshList() {
        list = employeeManagement.getEmployeeList();
    }
    
    private void setTmpDtoToModel() {
        if (tmpDto == null) {
            return;
        }
        empid = tmpDto.getEmpid();
        name = tmpDto.getName();
        // password = tmpDto.getPassword();
        email = tmpDto.getEmail();
        phone = tmpDto.getPhone();
        address = tmpDto.getAddress();
        appgroup = tmpDto.getAppgroup();
        active = tmpDto.getActive();
    }

}
